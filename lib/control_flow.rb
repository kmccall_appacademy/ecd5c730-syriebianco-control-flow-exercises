# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  rslt_str=""
  str.each_char do |ch|
    if ch==ch.upcase
      rslt_str+=ch
    end
  end
  rslt_str
end



# Return the middle character of a string. Return the middle two characters if the word is of even length, e.g. middle_substring("middle") => "dd", middle_substring("mid") => "i"
def middle_substring(str)
  half_len=str.length/2.0
  if str.length.odd?
    return str[half_len]
  else
    return str[half_len-1,2]
  end
end

# Return the number of vowels in a string.
def num_vowels(str)
  vowels=["a","e","i","o","u"]
  count=str.downcase.count(vowels.join)
end


# Return the factoral of the argument (num). A number's factorial is the product of all whole numbers between 1 and the number itself. Assume the argument will be > 0.
def factorial(num)
  product=1
  next_num=1
  while next_num<=num
    product=product*next_num
    puts product
    next_num+=1
  end
  product
end




# MEDIUM

# Write your own version of the join method. separator="" ensures that the default argument is "".
def my_join(arr, separator="")
  together=""
  arr.each_index do |idx|
    together+=arr[idx].to_s
    together+=separator unless idx==arr.length-1
  end
  together
end

# Write a method that converts its argument to weirdcase, where every odd character is lowercase and every even is uppercase, e.g. weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  cased=""
  arr=str.chars
  arr.each_index do |idx|
    if idx.even?
      cased+=arr[idx].to_s.downcase
    else
      cased+=arr[idx].to_s.upcase
    end
  end
  cased
end

# Reverse all words of five more more letters in a string. Return the resulting string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like my luck has desrever")
def reverse_five(str)
  fiversed=""
  word_array=str.split
  word_array.each do |el|
    if el.length>=5
      fiversed+=el.to_s.reverse
    else
      fiversed+=el.to_s
    end
    fiversed+=" " unless word_array.index(el)==word_array.length-1
  end
  fiversed
end


# Return an array of integers from 1 to 30 (inclusive), except for each multiple of 3 replace the integer with "fizz", for each multiple of 5 replace the integer with "buzz", and for each multiple of both 3 and 5, replace the integer with "fizzbuzz".
def fizzbuzz
  buzzed=[]
  (1..30).each do |el|
    if el%5==0&&el%3==0
      buzzed<<"fizzbuzz"
    elsif el%3==0
      buzzed<<"fizz"
    elsif el%5==0
      buzzed<<"buzz"
    else
      buzzed<<el
    end
  end
  buzzed
end


# HARD

# Write a method that returns a new array containing all the elements of the original array in reverse order.
def my_reverse(arr)
  reversed=[]
  idx=-1
  until idx==-arr.length-1
    reversed<<arr[idx]
    idx+=-1
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is prime.
def prime?(num)
  factors_count=0
  (1..num).each do |int|
    if num%int==0
      factors_count+=1
    end
  end
  factors_count==2
end


# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr=[]
  (1..num).each do |int|
    if num%int==0
      factors_arr<<int
    end
  end
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors=[]
  factors=factors(num)
  factors.each do |el|
    if prime?(el)
      prime_factors<<el
    end
  end
  prime_factors
end


# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens=[]
  odds=[]
  arr.each do |el|
    if el.even?
      evens<<el
    else
      odds<<el
    end
  end
  if evens.length==1
    return evens[0]
  elsif odds.length==1
    return odds[0]
  else
    return "there is no oddball in this array"
  end
end
